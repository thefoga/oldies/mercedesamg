# Mercedes AMG Petronas Formula 1 2015 *Connectivity Innovation Prize*

# Rules
* everything is explained very well [here](http://prize.tatacommunications.com/)

# Solution
* simple, fast and optimized app to get a real view on how the pilot is doing
* highly customizable with many personal options and settings
* highly optimized telemetry software

![screenshot](screenshot.png)*Screenshot*
