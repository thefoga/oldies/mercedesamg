/*
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef PlotterTING_H
#define PlotterTING_H

#include <QMainWindow>
#include <QtWidgets>
#include <QTimer>
#include "qcustomplot.h"
#include "dashboard.h"


class Plotter : public QMainWindow
{
    Q_OBJECT

public:
    explicit Plotter(QWidget *parent = 0);
    //graphs
    PlotterFour *tyres;
    PlotterFour *brakes;
    PlotterOne *tyres_usage;
    PlotterOne *battery;
    PlotterOne *fuel;
    PlotterOne *power;

    //containers
    QWidget *tyres_container;
    QWidget *brakes_container;
    QWidget *tyres_usage_container;
    QWidget *battery_container;
    QWidget *fuel_container;
    QWidget *power_container;

    //functions
    void updateDataSlot(double tyre_fl,double tyre_fr,double tyre_rl,double tyre_rr,double brake_fl,double brake_fr,double brake_rl,double brake_rr,int battery,int fuel, double power);
    void resize(QSize new_size);

public slots:
    void resize(QResizeEvent* resize_event);
};

#endif
