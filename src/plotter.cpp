/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "plotter.h"
#include <QtCore>


Plotter::Plotter(QWidget *parent) : QMainWindow(parent){
    tyres_container = new QWidget(this);
    brakes_container = new QWidget(this);
    tyres_usage_container = new QWidget(this);
    battery_container = new QWidget(this);
    fuel_container = new QWidget(this);
    power_container = new QWidget(this);

    tyres = new PlotterFour("Tyres\ntemps", tyres_container);
    brakes = new PlotterFour("Brakes\ntemps", brakes_container);
    tyres_usage = new PlotterOne("Respiration", tyres_usage_container);
    battery = new PlotterOne("Battery", battery_container);
    fuel = new PlotterOne("Fuel", fuel_container);
    power = new PlotterOne("Power\n[Nm]", power_container);
}

void Plotter::updateDataSlot(double tyre_fl,double tyre_fr,double tyre_rl,double tyre_rr,double brake_fl,double brake_fr,double brake_rl,double brake_rr,int battery,int fuel, double power){

    this->tyres->setValue(tyre_fl, tyre_fr, tyre_rl, tyre_rr);
    this->brakes->setValue(brake_fl, brake_fr, brake_rl, brake_rr);
    this->tyres_usage->setValue((tyre_fl + tyre_fr + tyre_rl + tyre_rr)/4);
    this->battery->setValue(battery);
    this->fuel->setValue(fuel);
    this->power->setValue(power);
}

void Plotter::resize(QResizeEvent* resize_event){
    QSize new_size = resize_event->size();
    resize(QSize(new_size.width() * 9 / 10, new_size.height() * 8 / 10));
}

void Plotter::resize(QSize new_size){
    int dx = new_size.width() / 2;
    int dy = new_size.height() / 3;

    tyres_container->setGeometry(0, 0, dx, dy);
    brakes_container->setGeometry(0, dy, dx, dy);
    tyres_usage_container->setGeometry(0, 2* dy, dx, dy);
    battery_container->setGeometry(dx, 0, dx, dy);
    fuel_container->setGeometry(dx, dy, dx, dy);
    power_container->setGeometry(dx, 2 * dy, dx, dy);

    tyres->resize(QSize(dx, dy));
    brakes->resize(QSize(dx, dy));
    tyres_usage->resize(QSize(dx, dy));
    battery->resize(QSize(dx, dy));
    fuel->resize(QSize(dx, dy));
    power->resize(QSize(dx, dy));
}
