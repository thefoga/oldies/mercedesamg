/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "overview.h"
#include "plotter.h"


#define DEFAULT_SCREEN_WIDTH 1366
#define DEFAULT_SCREEN_HEIGHT 768

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    //actions
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;

    QAction *recordAct;
    QAction *playAct;
    QAction *pauseAct;

    QAction *editAct;

    QAction *aboutAct;
    QAction *helpAct;

    //menu
    QMenu *fileMenu;
    QMenu *recordMenu;
    QMenu *showMenu;
    QMenu *logMenu;
    QMenu *editMenu;
    QMenu *helpMenu;

    //layout
    QWidget *centralWidget;
    QTabWidget *top_tabs;
    QStatusBar *statusbar;

    //realtime data
    int timeInterval;

    //layoutsetup
    void setupAction();
    void setupMenu();
    void setupTab();

signals:
    void resizeEvent(QResizeEvent*);

private: //realtime data
    QTimer dataTimer;

    //window in tabs
    Overview *live_timing;
    Plotter *live_plotter;
    QMainWindow *tester;

private slots:
    void updateDataSlot();
    void setRefreshRate(int ms);

    //menu
    void newFile();
    void openFile();
    void save();
    void saveAs();
    void quit();

    void record();
    void play();
    void pause();

    void editDialog();

    void aboutDialog();
    void helpDialog();
};

#endif
