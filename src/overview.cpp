/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


 #include "overview.h"


Overview::Overview(QWidget *parent) : QMainWindow(parent){
    //make room for indicators
    indicators_room = new QWidget(this);
    charts_room = new QWidget(this);
    toolbar_room = new QWidget(this);
    laptime_room = new QWidget(this);
    pit_room = new QWidget(this);

    //create widgets
    indicators = new Dashboard(indicators_room);
    charts = new LiveChart(charts_room);
    top_toolbar = new LiveToolbar(toolbar_room);
    laptime = new LapTimeStats(laptime_room);
    pitstop_helper = new PitStopManager(pit_room);
}

void Overview::updateDataSlot(int rpm, int gear, double speed, int throttle, int brake, int battery, int fuel, int steering, double brake_fl, double brake_fr, double brake_rl, double brake_rr, double tyre_fl, double tyre_fr, double tyre_rl, double tyre_rr){
    this->top_toolbar->update();
    this->indicators->setValue(rpm, gear, speed, throttle, brake, battery, fuel, steering, brake_fl, brake_fr, brake_rl, brake_rr, tyre_fl, tyre_fr, tyre_rl, tyre_rr);
    this->charts->updateDataSlot(rpm, gear, speed, throttle, brake);
    this->laptime->update(throttle / 2, brake / 3, battery / 2);
}

void Overview::resize(QResizeEvent* resize_event){
    QSize new_size = resize_event->size();
    resize(499 * new_size / 500);
}

void Overview::resize(QSize new_size){
    //rooms
    toolbar_room->setGeometry(new_size.width()/80, new_size.height()/50,new_size.width()/3.5, new_size.height()/12);
    indicators_room->setGeometry(0, new_size.height()/12 + new_size.height()/50, new_size.width()/3.2, new_size.height()/1.5);
    charts_room->setGeometry(new_size.width()/3.17, new_size.height()/50, new_size.width()/3, new_size.height()/1.3);
    laptime_room->setGeometry(2 * new_size.width() / 3, new_size.height()/50, new_size.width()/3 - new_size.width()/100, new_size.height()/2.5);
    pit_room->setGeometry(2 * new_size.width() / 3, new_size.height()/2.4 + new_size.height()/50, new_size.width()/3 - new_size.width()/100, new_size.height()/3);

    //indicators
    top_toolbar->resize(QSize(new_size.width()/3.5, new_size.height()/12));
    indicators->resize(QSize(new_size.width()/3.2, new_size.height()/1.5));
    charts->resize(QSize(new_size.width()/3.1, new_size.height()/1.3));
    laptime->resize(QSize(new_size.width()/3 - new_size.width()/100, new_size.height()/2.5));
    pitstop_helper->resize(QSize(new_size.width()/3 - new_size.width()/100, new_size.height()/3));
}
