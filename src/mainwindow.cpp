/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent){
    this->setWindowTitle(QString::fromUtf8("Mercedes Petronas AMG Formula 1 Telemetry Software"));
    centralWidget = new QWidget(this);

    setupAction();
    setupMenu();
    setupTab();

    timeInterval = 20;
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(updateDataSlot()));
    dataTimer.start(timeInterval);

    this->setCentralWidget(centralWidget);
}

void MainWindow::setupAction(){
    //file menu
    newAct = new QAction(QPixmap("img/new.png"), "&New", this);
    newAct->setShortcuts(QKeySequence::New);
    QObject::connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    openAct = new QAction(QIcon("img/open.png"), "O&pen...", this);
    openAct->setShortcuts(QKeySequence::Open);
    QObject::connect(openAct, SIGNAL(triggered()), this, SLOT(openFile()));

    saveAct = new QAction(QIcon("img/save.png"), tr("Sa&ve"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    QObject::connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    QObject::connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    exitAct = new QAction(tr("Exit&..."), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    QObject::connect(exitAct, SIGNAL(triggered()), this, SLOT(quit()));


    //record menu
    recordAct = new QAction(QIcon("img/record.png"), "&Start recording..", this);
    QList<QKeySequence> record_shortcut;
    record_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_R));
    recordAct->setShortcuts(record_shortcut);
    QObject::connect(recordAct, SIGNAL(triggered()), this, SLOT(record()));

    playAct = new QAction(QIcon("img/play.png"), tr("P&lay"), this);
    QList<QKeySequence> play_shortcut;
    play_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_P));
    playAct->setShortcuts(play_shortcut);
    QObject::connect(playAct, SIGNAL(triggered()), this, SLOT(play()));

    pauseAct = new QAction(QIcon("img/pause.png"), "Pa&use", this);
    QList<QKeySequence> pause_shortcut;
    pause_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_R));
    pauseAct->setShortcuts(pause_shortcut);
    QObject::connect(pauseAct, SIGNAL(triggered()), this, SLOT(pause()));

    //edit menu
    editAct = new QAction(QPixmap("img/settings.png"), "&Settings", this);
    QList<QKeySequence> edit_shortcut;
    edit_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_E));
    editAct->setShortcuts(edit_shortcut);
    QObject::connect(editAct, SIGNAL(triggered()), this, SLOT(editDialog()));

    //about menu
    aboutAct = new QAction(QPixmap("img/about.png"), "&About", this);
    QList<QKeySequence> about_shortcut;
    about_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_A));
    aboutAct->setShortcuts(about_shortcut);
    QObject::connect(aboutAct, SIGNAL(triggered()), this, SLOT(aboutDialog()));

    helpAct = new QAction(QPixmap("img/help.png"), "H&elp", this);
    QList<QKeySequence> help_shortcut;
    help_shortcut.append(QKeySequence(Qt::CTRL + Qt::Key_H));
    helpAct->setShortcuts(help_shortcut);
    QObject::connect(helpAct, SIGNAL(triggered()), this, SLOT(helpDialog()));
}

void MainWindow::setupMenu(){
    fileMenu = this->menuBar()->addMenu("&File");
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);
    this->menuBar()->addSeparator();

    recordMenu = this->menuBar()->addMenu("R&ecord");
    recordMenu->addAction(recordAct);
    recordMenu->addAction(playAct);
    recordMenu->addAction(pauseAct);
    this->menuBar()->addSeparator();

    showMenu = this->menuBar()->addMenu("Sh&ow");
    this->menuBar()->addSeparator();

    logMenu = this->menuBar()->addMenu("Log&");
    this->menuBar()->addSeparator();

    editMenu = this->menuBar()->addMenu("Ed&it");
    editMenu->addAction(editAct);
    this->menuBar()->addSeparator();

    helpMenu = this->menuBar()->addMenu("Hel&p");
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(helpAct);

    statusbar = new QStatusBar();
    statusBar()->showMessage("Starting pseudo-random data sampling...");
}

void MainWindow::setupTab(){
    top_tabs = new QTabWidget(centralWidget);
    live_timing = new Overview();
    live_plotter = new Plotter();
    top_tabs->addTab(live_timing,"Live Timing");
    top_tabs->addTab(live_plotter,"Live Plotting");

    //resizing
    QDesktopWidget desktop;
    int desktopHeight = desktop.geometry().height();
    int desktopWidth = desktop.geometry().width();
    top_tabs->setMinimumSize(desktopWidth, desktopHeight);

    //connect tabs to mainwindow
    QObject::connect(this, SIGNAL(resizeEvent(QResizeEvent*)), this->live_timing, SLOT(resize(QResizeEvent*)));
    QObject::connect(this, SIGNAL(resizeEvent(QResizeEvent*)), this->live_plotter, SLOT(resize(QResizeEvent*)));
    //showMenu->addAction(this->live_timing->dock->toggleViewAction());
}

void MainWindow::editDialog(){
    statusBar()->showMessage("Edit settings...");
    QWidget *dialog = new QWidget;
    dialog->setWindowTitle("Session preferences");

    //create spin box, entry value and button
    QSpinBox *spinner = new QSpinBox(dialog);
    spinner->setRange(0, 2000);
    spinner->setValue(this->timeInterval);

    QSlider *slider = new QSlider(Qt::Horizontal, dialog);
    slider->setRange(0, 2000);
    slider->setTickInterval(10);

    QLabel *istruction = new QLabel(dialog);
    istruction->setText("Set refresh rate;\n0 means as fast as possible.");

    //connect spinner and slider
    QObject::connect(spinner, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
    QObject::connect(slider, SIGNAL(valueChanged(int)), spinner, SLOT(setValue(int)));

    //accept button
    QPushButton *accept = new QPushButton(dialog);
    accept->setText("Ok");
    QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setRefreshRate(int)));
    QObject::connect(accept, SIGNAL(clicked()), dialog, SLOT(close()));

    //setting up layout
    QDesktopWidget desktop;
    int desktopHeight = desktop.geometry().height();
    int desktopWidth = desktop.geometry().width();
    int dx = 20 * desktopWidth / DEFAULT_SCREEN_WIDTH;
    int dy = 20 * desktopHeight / DEFAULT_SCREEN_HEIGHT;

    istruction->setGeometry(dx,dy,10*dx,3*dy);
    spinner->setGeometry(2*dx,4*dy,3*dx,2*dy);
    slider->setGeometry(6*dx,4*dy,6*dx,2*dy);
    accept->setGeometry(4*dx,7*dy,2*dx,1*dy);
    dialog->setMinimumSize(10*dx,9*dy);
    dialog->show();
}

void MainWindow::aboutDialog(){
    statusBar()->showMessage("About this software...");
    QString aboutText = "This software is a sample telemetry software developed for Mercedes Petronas AMG Formula 1.\n"
                        "Copyright (C) 2015. Created by sirfoga. All right Reserved.\n"
                        "Contact developer: sirfoga@gmail.com";
    QMessageBox::about(this, "Mercedes Petronas AMG Formula 1 Telemetry Software", aboutText);

}

void MainWindow::helpDialog(){
    statusBar()->showMessage("Help!...");
    QString aboutText = "All you need is love";
    QMessageBox::about(this, "Is something wrong?", aboutText);
}

void MainWindow::updateDataSlot(){
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    int rpm = qrand() % ((15000 + 0) - 1) + 1;
    int gear = qrand() % ((8 + 0) - 1) + 0;
    double speed = qrand() % ((360 + 0) - 1) + 0;
    int throttle = qrand() % ((100 + 0) - 1) + 0;
    int brake = qrand() % ((100 + 0) - 1) + 0;
    int battery = qrand() % ((100 + 0) - 1) + 0;
    int fuel = qrand() % ((100 + 0) - 1) + 0;
    int steering = qrand() % ((180 + 0) - 1) + 0 - 90;
    double brake_fl = qrand() % ((1000 + 0) - 1) + 0;
    double brake_fr = qrand() % ((1000 + 0) - 1) + 0;
    double brake_rl = qrand() % ((1000 + 0) - 1) + 0;
    double brake_rr = qrand() % ((1000 + 0) - 1) + 0;
    double tyre_fl = qrand() % ((120 + 0) - 1) + 0;
    double tyre_fr = qrand() % ((120 + 0) - 1) + 0;
    double tyre_rl = qrand() % ((120 + 0) - 1) + 0;
    double tyre_rr = qrand() % ((120 + 0) - 1) + 0;
    double power = qrand() % ((1000 + 0) - 1) + 0;

    this->live_timing->updateDataSlot(rpm, gear, speed, throttle, brake, battery, fuel, steering, brake_fl, brake_fr, brake_rl, brake_rr, tyre_fl, tyre_fr, tyre_rl, tyre_rr);
    this->live_plotter->updateDataSlot(tyre_fl, tyre_fr, tyre_rl, tyre_rr, brake_fl, brake_fr, brake_rl, brake_rr, battery, fuel, power);
}

void MainWindow::setRefreshRate(int ms){
    this->timeInterval = ms;
    this->dataTimer.start(this->timeInterval);
}

void MainWindow::newFile(){
    //
}

void MainWindow::openFile(){
    //
}

void MainWindow::save(){
    //
}

void MainWindow::saveAs(){
    //
}

void MainWindow::record(){
    //
}

void MainWindow::play(){
    //
}

void MainWindow::pause(){
    //
}

void MainWindow::quit(){
    this->quit();
}
