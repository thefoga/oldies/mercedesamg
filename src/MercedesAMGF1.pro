#-------------------------------------------------
#
# Project created by QtCreator 2015-06-27T16:07:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = Mercedes_AMG_F1
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    qcustomplot.cpp \
    overview.cpp \
    plotter.cpp \
    QRoundProgressBar.cpp \

HEADERS += mainwindow.h \
    qcustomplot.h \
    plotter.h \
    dashboard.h \
    QRoundProgressBar.h \
    overview.h

FORMS +=

HEADERS += \
    dashboard.h \
    mainwindow.h \
    overview.h \
    plotter.h \
    qcustomplot.h \
    QRoundProgressBar.h
