/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef OVERVIEW_H
#define OVERVIEW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QTimer>
#include <QLCDNumber>
#include "dashboard.h"


class Overview : public QMainWindow
{
    Q_OBJECT

public:
    explicit Overview(QWidget *parent = 0);

    //visual indicators
    Dashboard *indicators;
    LiveChart *charts;
    LiveToolbar *top_toolbar;
    LapTimeStats *laptime;
    PitStopManager *pitstop_helper;

    //room for indicators
    QWidget *indicators_room;
    QWidget *charts_room;
    QWidget *toolbar_room;
    QWidget *laptime_room;
    QWidget *pit_room;

public slots:
    void updateDataSlot(int rpm, int gear, double speed, int throttle, int brake, int battery, int fuel, int steering, double brake_fl, double brake_fr, double brake_rl, double brake_rr, double tyre_fl, double tyre_fr, double tyre_rl, double tyre_r);
    void resize(QResizeEvent* resize_event);
    void resize(QSize new_size);
};

#endif
