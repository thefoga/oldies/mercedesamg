/**
 * Copyright 2016 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef DASHBOARD_H
#define DASHBOARD_H

#include <QtWidgets>
#include "QRoundProgressBar.h"
#include "qcustomplot.h"


#define DEFAULT_SCREEN_WIDTH 1366
#define DEFAULT_SCREEN_HEIGHT 768

/*display RPM graphics, size (33:5)
 *   RPM : 13567
 * ooooo ooooo ooooo
*/
class RpmIndicator : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    QLabel *value;
    QProgressBar *low0;
    QProgressBar *low1;
    QProgressBar *low2;
    QProgressBar *low3;
    QProgressBar *low4;
    QProgressBar *med0;
    QProgressBar *med1;
    QProgressBar *med2;
    QProgressBar *med3;
    QProgressBar *med4;
    QProgressBar *high0;
    QProgressBar *high1;
    QProgressBar *high2;
    QProgressBar *high3;
    QProgressBar *high4;

    QFrame *frame;

    //class f
    RpmIndicator(QWidget *parent){
        central = new QWidget(parent);
        setupLayout();
        setupBar();
        setValue(0);
    }

    void setupLayout(){
        value = new QLabel(central);
        value->setAlignment(Qt::AlignCenter);

        low0 = new QProgressBar(central);
        low1 = new QProgressBar(central);
        low2 = new QProgressBar(central);
        low3 = new QProgressBar(central);
        low4 = new QProgressBar(central);
        med0 = new QProgressBar(central);
        med1 = new QProgressBar(central);
        med2 = new QProgressBar(central);
        med3 = new QProgressBar(central);
        med4 = new QProgressBar(central);
        high0 = new QProgressBar(central);
        high1 = new QProgressBar(central);
        high2 = new QProgressBar(central);
        high3 = new QProgressBar(central);
        high4 = new QProgressBar(central);

        frame = new QFrame(central);
        frame->setFrameStyle(QFrame::Box);
    }

    void setupBar(){
        QString low_rpm = "QProgressBar::chunk {"
                          "background: #33CC00;"
                          "border-radius: 19px}"
                          "QProgressBar{"
                          "background: #E0E0E0;"
                          "border-radius: 19px}";
        QString med_rpm = "QProgressBar::chunk {"
                          "background: #FF0000;"
                          "border-radius: 19px;}"
                          "QProgressBar{"
                          "background: #E0E0E0;"
                          "border-radius: 19px}";
        QString high_rpm = "QProgressBar::chunk {"
                           "background:  #0000FF;"
                           "border-radius: 19px;}"
                           "QProgressBar{"
                           "background: #E0E0E0;"
                           "border-radius: 19px}";

        low0->setStyleSheet(low_rpm);
        low1->setStyleSheet(low_rpm);
        low2->setStyleSheet(low_rpm);
        low3->setStyleSheet(low_rpm);
        low4->setStyleSheet(low_rpm);
        med0->setStyleSheet(med_rpm);
        med1->setStyleSheet(med_rpm);
        med2->setStyleSheet(med_rpm);
        med3->setStyleSheet(med_rpm);
        med4->setStyleSheet(med_rpm);
        high0->setStyleSheet(high_rpm);
        high1->setStyleSheet(high_rpm);
        high2->setStyleSheet(high_rpm);
        high3->setStyleSheet(high_rpm);
        high4->setStyleSheet(high_rpm);

        low0->setTextVisible(false);
        low1->setTextVisible(false);
        low2->setTextVisible(false);
        low3->setTextVisible(false);
        low4->setTextVisible(false);
        med0->setTextVisible(false);
        med1->setTextVisible(false);
        med2->setTextVisible(false);
        med3->setTextVisible(false);
        med4->setTextVisible(false);
        high0->setTextVisible(false);
        high1->setTextVisible(false);
        high2->setTextVisible(false);
        high3->setTextVisible(false);
        high4->setTextVisible(false);

        low0->setRange(0,1000);
        low1->setRange(0,1000);
        low2->setRange(0,1000);
        low3->setRange(0,1000);
        low4->setRange(0,1000);
        med0->setRange(0,1000);
        med1->setRange(0,1000);
        med2->setRange(0,1000);
        med3->setRange(0,1000);
        med4->setRange(0,1000);
        high0->setRange(0,1000);
        high1->setRange(0,1000);
        high2->setRange(0,1000);
        high3->setRange(0,1000);
        high4->setRange(0,1000);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 33;
        int dy = new_size.height() / 5;

        central->setMinimumSize(33*dx,5*dy);
        central->setMaximumSize(33*dx,5*dy);

        QFont resized_font;
        resized_font.setPointSize(15*dy/20);
        value->setGeometry(0,1*dy,33*dx,dy);
        value->setFont(resized_font);

        low0->setGeometry(1*dx,3*dy,1*dx,1*dy);
        low1->setGeometry(3*dx,3*dy,1*dx,1*dy);
        low2->setGeometry(5*dx,3*dy,1*dx,1*dy);
        low3->setGeometry(7*dx,3*dy,1*dx,1*dy);
        low4->setGeometry(9*dx,3*dy,1*dx,1*dy);
        med0->setGeometry(12*dx,3*dy,1*dx,1*dy);
        med1->setGeometry(14*dx,3*dy,1*dx,1*dy);
        med2->setGeometry(16*dx,3*dy,1*dx,1*dy);
        med3->setGeometry(18*dx,3*dy,1*dx,1*dy);
        med4->setGeometry(20*dx,3*dy,1*dx,1*dy);
        high0->setGeometry(23*dx,3*dy,1*dx,1*dy);
        high1->setGeometry(25*dx,3*dy,1*dx,1*dy);
        high2->setGeometry(27*dx,3*dy,1*dx,1*dy);
        high3->setGeometry(29*dx,3*dy,1*dx,1*dy);
        high4->setGeometry(31*dx,3*dy,1*dx,1*dy);

        frame->setGeometry(0,0,33*dx,5*dy);

        if(dx < 8){
            //resize border
            QString low_rpm = "QProgressBar::chunk {"
                              "background: #33CC00;"
                              "border-radius: 3px}"
                              "QProgressBar{"
                              "background: #E0E0E0;"
                              "border-radius: 3px}";
            QString med_rpm = "QProgressBar::chunk {"
                              "background: #FF0000;"
                              "border-radius: 3px;}"
                              "QProgressBar{"
                              "background: #E0E0E0;"
                              "border-radius: 3px}";
            QString high_rpm = "QProgressBar::chunk {"
                               "background:  #0000FF;"
                               "border-radius: 3px;}"
                               "QProgressBar{"
                               "background: #E0E0E0;"
                               "border-radius: 3px}";
            low0->setStyleSheet(low_rpm);
            low1->setStyleSheet(low_rpm);
            low2->setStyleSheet(low_rpm);
            low3->setStyleSheet(low_rpm);
            low4->setStyleSheet(low_rpm);
            med0->setStyleSheet(med_rpm);
            med1->setStyleSheet(med_rpm);
            med2->setStyleSheet(med_rpm);
            med3->setStyleSheet(med_rpm);
            med4->setStyleSheet(med_rpm);
            high0->setStyleSheet(high_rpm);
            high1->setStyleSheet(high_rpm);
            high2->setStyleSheet(high_rpm);
            high3->setStyleSheet(high_rpm);
            high4->setStyleSheet(high_rpm);
        }
    }

    void setValue(int value){
        this->value->setText("Engine [rpm] : " + QString::number(value));

        if(value < 0){
            low0->setValue(0);
            low1->setValue(0);
            low2->setValue(0);
            low3->setValue(0);
            low4->setValue(0);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 1000){
            low0->setValue(value);
            low1->setValue(0);
            low2->setValue(0);
            low3->setValue(0);
            low4->setValue(0);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 2000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(0);
            low3->setValue(0);
            low4->setValue(0);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 3000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(0);
            low4->setValue(0);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 4000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(0);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 5000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(0);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 6000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(0);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 7000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(0);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 8000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(0);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 9000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(0);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 10000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(0);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 11000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(1000);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 12000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(1000);
            high1->setValue(0);
            high2->setValue(0);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 13000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(1000);
            high1->setValue(1000);
            high2->setValue(1000);
            high3->setValue(0);
            high4->setValue(0);
        }else if(value < 14000){
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(1000);
            high1->setValue(1000);
            high2->setValue(1000);
            high3->setValue(1000);
            high4->setValue(0);
        }else{
            low0->setValue(1000);
            low1->setValue(1000);
            low2->setValue(1000);
            low3->setValue(1000);
            low4->setValue(1000);
            med0->setValue(1000);
            med1->setValue(1000);
            med2->setValue(1000);
            med3->setValue(1000);
            med4->setValue(1000);
            high0->setValue(1000);
            high1->setValue(1000);
            high2->setValue(1000);
            high3->setValue(1000);
            high4->setValue(1000);
        }
    }
};

/*display gear, speed and accelerometer graphics, size (26:14)
 *   _      ____
 *  |_|   _|___|_
 * |   | | || || |
 * |___| |_||_||_|
*/
class SpeedIndicator : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;

    QLCDNumber *gear;
    QRoundProgressBar *speed;
    QProgressBar *throttle;
    QProgressBar *brake;
    QProgressBar *fuel;
    QProgressBar *battery;

    QLabel *speed_text;
    QLabel *battery_level;
    QLabel *throttle_level;
    QLabel *brake_level;
    QLabel *fuel_level;

    QFrame *frame_speed;
    QFrame *frame_accelerator;

    //class f
    SpeedIndicator(QWidget *parent){
        central = new QWidget(parent);

        setupLayout();
        setupIndicators();
        setValue(0,0,0,0,0,0);
    }

    void setupLayout(){
        gear = new QLCDNumber(central);
        speed = new QRoundProgressBar(central);
        throttle = new QProgressBar(central);
        brake = new QProgressBar(central);
        battery = new QProgressBar(central);
        fuel = new QProgressBar(central);

        speed_text = new QLabel("Gear\nSpeed [KMH]", central);
        battery_level = new QLabel("Battery level [%]", central);
        throttle_level = new QLabel("Throttle [%]", central);
        brake_level = new QLabel("Brake [%]", central);
        fuel_level = new QLabel("Fuel [%]", central);

        speed_text->setAlignment(Qt::AlignCenter);
        battery_level->setAlignment(Qt::AlignCenter);
        throttle_level->setAlignment(Qt::AlignCenter);
        brake_level->setAlignment(Qt::AlignCenter);
        fuel_level->setAlignment(Qt::AlignCenter);

        frame_speed = new QFrame(central);
        frame_speed->setFrameStyle(QFrame::Box);
        frame_accelerator = new QFrame(central);
        frame_accelerator->setFrameStyle(QFrame::Box);
    }

    void setupIndicators(){
        QString red_brake = "QProgressBar::chunk {"
                          "background: #FF0000;"
                          "border-radius: 5px;}"
                          "QProgressBar{"
                          "background: #E0E0E0}";
        QString green_throttle = "QProgressBar::chunk {"
                          "background: #33CC00;"
                          "border-radius: 5px;}"
                          "QProgressBar{"
                          "background: #E0E0E0}";
        QString fuel_color = "QProgressBar::chunk {"
                             "background: #B88A00;"
                             "border-radius: 5px;}"
                             "QProgressBar{"
                             "background: #E0E0E0}";

        gear->setDigitCount(1);
        gear->setSegmentStyle(QLCDNumber::Flat);

        speed->setMinimum(0);
        speed->setMaximum(360);
        speed->setFormat("%v");
        speed->setDataPenWidth(10);
        speed->setBarStyle(QRoundProgressBar::StyleLine);
        speed->setDecimals(0);

        //color
        QPalette palette_speed;
        palette_speed.setBrush(QPalette::Active, QPalette::Highlight, Qt::black);
        speed->setPalette(palette_speed);

        throttle->setRange(0,100);
        throttle->setMinimum(0);
        throttle->setFormat("%v");
        throttle->setStyleSheet(green_throttle);
        throttle->setOrientation(Qt::Vertical);
        throttle->setAlignment(Qt::AlignCenter);

        brake->setRange(0,100);
        brake->setMinimum(0);
        brake->setFormat("%v");
        brake->setInvertedAppearance(true);
        brake->setStyleSheet(red_brake);
        brake->setOrientation(Qt::Vertical);
        brake->setAlignment(Qt::AlignCenter);

        battery->setRange(0,100);
        battery->setFormat("%p");
        battery->setOrientation(Qt::Horizontal);
        battery->setAlignment(Qt::AlignCenter);

        fuel->setRange(0,100);
        fuel->setMinimum(0);
        fuel->setFormat("%v");
        fuel->setStyleSheet(fuel_color);
        fuel->setOrientation(Qt::Vertical);
        fuel->setAlignment(Qt::AlignCenter);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 26;
        int dy = new_size.height() / 14;

        central->setMinimumSize(26*dx,14*dy);
        central->setMaximumSize(26*dx,14*dy);

        gear->setGeometry(7*dx,1*dy,2*dx,2*dy);

        speed->setOutlinePenWidth(4 * dx / 5);
        speed->setGeometry(1*dx,4*dy,9*dx,9*dy);
        battery->setGeometry(11*dx,3*dy,14*dx,2*dy);
        throttle->setGeometry(11*dx,7*dy,4*dx,6*dy);
        brake->setGeometry(16*dx,7*dy,4*dx,6*dy);
        fuel->setGeometry(21*dx, 7*dy, 4*dx, 6*dy);

        speed_text->setGeometry(1*dx,1*dy,6*dx,2*dy);
        battery_level->setGeometry(11*dx,1*dy,14*dx,1*dy);
        throttle_level->setGeometry(11*dx,5*dy,4*dx,1*dy);
        brake_level->setGeometry(16*dx,5*dy,4*dx,1*dy);
        fuel_level->setGeometry(21*dx, 5*dy, 4*dx, 1*dy);

        frame_speed->setGeometry(0,0, 10*dx,14*dy);
        frame_accelerator->setGeometry(10*dx,0, 16*dx,14*dy);

        QFont resized_font;
        resized_font.setPointSize(15*dy/20);
        speed_text->setFont(resized_font);
        battery_level->setFont(resized_font);
        throttle_level->setFont(resized_font);
        brake_level->setFont(resized_font);
        fuel_level->setFont(resized_font);
    }

    void setValue(int gear, int speed, int throttle, int brake, int battery, int fuel){
        this->gear->display(gear);
        this->speed->setValue(floor(speed));
        this->throttle->setValue(throttle);
        this->brake->setValue(brake);
        this->battery->setValue(battery);
        this->fuel->setValue(fuel);

        QString full_battery = "QProgressBar::chunk {"
                          "background: #33CC00;"
                          "border-radius: 5px;}"
                          "QProgressBar{"
                          "background: #E0E0E0}";
        QString med_battery = "QProgressBar::chunk {"
                          "background: #FFFF00;"
                          "border-radius: 5px;}"
                          "QProgressBar{"
                          "background: #E0E0E0}";
        QString low_battery = "QProgressBar::chunk {"
                          "background: #FF0000;"
                          "border-radius: 5px;}"
                          "QProgressBar{"
                          "background: #FF0000}";

        if(battery < 33){
            this->battery->setStyleSheet(low_battery);
        }else if(battery < 67){
            this->battery->setStyleSheet(med_battery);
        }else{
            this->battery->setStyleSheet(full_battery);
        }
    }
};

/*displays temperatures of brakes and tyres, size (10:6)
 *  _______
 * | Temps |
 * |___|___|
 * |___|___|
*/
class TemperaturesWheels : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;

    //labels
    QLabel *title;
    QLabel *temp_fl;
    QLabel *temp_fr;
    QLabel *temp_rl;
    QLabel *temp_rr;

    //frame
    QFrame *fl;
    QFrame *fr;
    QFrame *rl;
    QFrame *rr;

    //class f
    TemperaturesWheels(QString title, QWidget *parent){
        central = new QWidget(parent);
        this->title = new QLabel(title, central);

        setupLayout();
        setValue(0,0,0,0);
    }

    void setupLayout(){
        this->title->setAlignment(Qt::AlignCenter);
        temp_fl = new QLabel(central);
        temp_fr = new QLabel(central);
        temp_rl = new QLabel(central);
        temp_rr = new QLabel(central);

        temp_fl->setAlignment(Qt::AlignCenter);
        temp_fr->setAlignment(Qt::AlignCenter);
        temp_rl->setAlignment(Qt::AlignCenter);
        temp_rr->setAlignment(Qt::AlignCenter);

        fl = new QFrame(central);
        fr = new QFrame(central);
        rl = new QFrame(central);
        rr = new QFrame(central);

        fr->setFrameStyle(QFrame::Box);
        fl->setFrameStyle(QFrame::Box);
        rl->setFrameStyle(QFrame::Box);
        rr->setFrameStyle(QFrame::Box);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 10;
        int dy = new_size.height() / 6;

        central->setMinimumSize(10*dx ,6*dy);
        central->setMaximumSize(10*dx ,6*dy);

        title->setGeometry(0,0,10*dx,2*dy);
        temp_fl->setGeometry(0,2*dy,5*dx,2*dy);
        temp_fr->setGeometry(5*dx,2*dy,5*dx,2*dy);
        temp_rl->setGeometry(0,4*dy,5*dx,2*dy);
        temp_rr->setGeometry(5*dx,4*dy,5*dx,2*dy);

        fl->setGeometry(0,2*dy,5*dx,2*dy);
        fr->setGeometry(5*dx,2*dy,5*dx,2*dy);
        rl->setGeometry(0,4*dy,5*dx,2*dy);
        rr->setGeometry(5*dx,4*dy,5*dx,2*dy);

        QFont resized_font;
        resized_font.setPointSize(15*dy/20);
        this->title->setFont(resized_font);
        temp_fl->setFont(resized_font);
        temp_fr->setFont(resized_font);
        temp_rl->setFont(resized_font);
        temp_rr->setFont(resized_font);
    }

    void setValue(int temp_fl, int temp_fr, int temp_rl, int temp_rr){
        //update data
        this->temp_fl->setText(QString::number(temp_fl) + " °C");
        this->temp_fr->setText(QString::number(temp_fr) + " °C");
        this->temp_rl->setText(QString::number(temp_rl) + " °C");
        this->temp_rr->setText(QString::number(temp_rr) + " °C");
        QString low_temp = "QLabel {"
                            "border: 2px solid black;"
                            "background-color : green;"
                            "color : white;"
                            "}";
        QString med_temp = "QLabel {"
                            "border: 2px solid black;"
                            "background-color : yellow;"
                            "color : black;"
                            "}";
        QString high_temp = "QLabel {"
                            "border: 2px solid black;"
                            "background-color : red;"
                            "color : white;"
                            "}";
        //update color
        if(temp_fl < 300){
            this->temp_fl->setStyleSheet(low_temp);
        }else if(temp_fl < 667){
            this->temp_fl->setStyleSheet(med_temp);
        }else{
            //temp_fl < 1000
            this->temp_fl->setStyleSheet(high_temp);
        }

        if(temp_fr < 300){
            this->temp_fr->setStyleSheet(low_temp);
        }else if(temp_fr < 667){
            this->temp_fr->setStyleSheet(med_temp);
        }else{
            //temp_fr < 1000
            this->temp_fr->setStyleSheet(high_temp);
        }

        if(temp_rl < 300){
            this->temp_rl->setStyleSheet(low_temp);
        }else if(temp_rl < 667){
            this->temp_rl->setStyleSheet(med_temp);
        }else{
            //temp_rl < 1000
            this->temp_rl->setStyleSheet(high_temp);
        }

        if(temp_rr < 300){
            this->temp_rr->setStyleSheet(low_temp);
        }else if(temp_rr < 667){
            this->temp_rr->setStyleSheet(med_temp);
        }else{
            //temp_rr < 1000
            this->temp_rr->setStyleSheet(high_temp);
        }
    }
};

/*displays dashboard with full indicators, size (32:27)
 *    RPM : 13567
 * ooooo ooooo ooooo
 *    _      ____
 *   |_|   _|___|_
 *  |   | | || || |
 *  |___| |_||_||_|
 *   ___   _______
 *  |   | | Temps |
 *  |   | |___|___|
 *  |___| |___|___|
*/
class Dashboard : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    RpmIndicator *rpm;
    SpeedIndicator *speed;
    QLabel *steering_value;
    QLabel *steering_wheel;
    QPixmap wheel;
    TemperaturesWheels *brakes;
    TemperaturesWheels *tyres;

    QWidget *rpm_room;
    QWidget *speed_room;
    QWidget *brakes_room;
    QWidget *tyres_room;

    //frame
    QFrame *steer_frame;

    Dashboard(QWidget *parent){
        central = new QWidget(parent);
        setupLayout();
        setValue(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    }

    void setupLayout(){
        rpm_room = new QWidget(central);
        speed_room = new QWidget(central);

        steering_value = new QLabel("Angle: 0°", central);
        steering_value->setAlignment(Qt::AlignCenter);
        steering_wheel = new QLabel(central);
        wheel = QPixmap("img/wheel.png");

        brakes_room = new QWidget(central);
        tyres_room = new QWidget(central);

        rpm = new RpmIndicator(rpm_room);
        speed = new SpeedIndicator(speed_room);
        brakes = new TemperaturesWheels("Brakes Temps" , brakes_room);
        tyres = new TemperaturesWheels("Tyres Temps" , tyres_room);

        steer_frame = new QFrame(central);
        steer_frame->setFrameStyle(QFrame::Box);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 33;
        int dy = new_size.height() / 34;

        central->setMinimumSize(34*dx,34*dy);
        central->setMaximumSize(34*dx,34*dy);

        rpm_room->setGeometry(2.5*dx,0, 32*dx,5*dy);
        speed_room->setGeometry(dx,6*dy,34*dx,13*dy);

        QFont resized_font;
        resized_font.setPointSize(15*dy/20);
        steering_value->setFont(resized_font);
        steering_value->setGeometry(dx,21*dy,13*dx,2*dy);
        wheel = QPixmap("img/wheel.png");
        wheel = wheel.scaledToWidth(9*dx);
        steering_wheel->setPixmap(wheel);
        steering_wheel->setGeometry(2*dx, 23*dy,13*dx,10*dy);

        brakes_room->setGeometry(17*dx,19*dy,15*dx,6*dy);
        tyres_room->setGeometry(17*dx,27*dy,15*dx,6*dy);

        rpm->resize(QSize(32*dx,5*dy));
        speed->resize(QSize(34*dx,13*dy));
        brakes->resize(QSize(15*dx,6*dy));
        tyres->resize(QSize(15*dx,6*dy));

        steer_frame->setGeometry(dx, 20*dy,12.5*dx,13*dy);
    }

    void setValue(int rpm, int gear, double speed, int throttle, int brake, int battery, int fuel, int steering, double brake_fl, double brake_fr, double brake_rl, double brake_rr, double tyre_fl, double tyre_fr, double tyre_rl, double tyre_rr){
        this->rpm->setValue(rpm);
        this->speed->setValue(gear, speed, throttle, brake, battery, fuel);

        steering_value->setText("Steering angle:\n" + QString::number(steering) + " °");
        QTransform transform;
        steering_wheel->setPixmap(wheel.transformed(transform.rotate(steering)));

        this->brakes->setValue(brake_fl, brake_fr, brake_rl, brake_rr);
        this->tyres->setValue(tyre_fl, tyre_fr, tyre_rl, tyre_rr);
    }
};

/*  ______
 * |______|
 * |______|
 * |______|
 * |______|
*/
//displays live charts of 4 data source
class LiveChart : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    QSplitter *splitter;
    QCustomPlot *rpm;
    QCustomPlot *speed;
    QCustomPlot *gear;
    QCustomPlot *accelerator;

    LiveChart(QWidget *parent){
        central = new QWidget(parent);
        setupLayout();

        //setup plots
        setupPlot(rpm);
        setupPlot(speed);
        setupPlot(gear);
        setupPlot(accelerator);

        //setup plots' labels
        rpm->xAxis->setLabel("RPM [rpm]");
        speed->xAxis->setLabel("Speed [KMH]");
        gear->xAxis->setLabel("GEAR");
        accelerator->xAxis->setLabel("THROTTLE/BRAKE");
    }

    void setupLayout(){
        splitter = new QSplitter(central);
        splitter->setOrientation(Qt::Vertical);

        rpm = new QCustomPlot(central);
        speed = new QCustomPlot(central);
        gear = new QCustomPlot(central);
        accelerator = new QCustomPlot(central);

        splitter->addWidget(rpm);
        splitter->addWidget(speed);
        splitter->addWidget(gear);
        splitter->addWidget(accelerator);

        splitter->setGeometry(0,0,central->size().width(), central->size().height());
    }

    void setupPlot(QCustomPlot *customPlot){
        if(customPlot == this->accelerator){
            //2 plots
            customPlot->addGraph();//throttle
            customPlot->graph(0)->setPen(QPen(QColor("#33CC00")));

            customPlot->addGraph(); //brake
            customPlot->graph(1)->setPen(QPen(QColor("#FF0000")));

            //customPlot->xAxis->setLabel("Throttle");
            customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
            customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
            customPlot->xAxis->setAutoTickStep(false);
            customPlot->xAxis->setTickStep(2);
            customPlot->axisRect()->setupFullAxesBox();

            // make left and bottom axes transfer their ranges to right and top axes:
            connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
            connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
        }else{
            customPlot->addGraph();
            customPlot->graph(0)->setPen(QPen(Qt::blue));

            customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
            customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
            customPlot->xAxis->setAutoTickStep(false);
            customPlot->xAxis->setTickStep(2);
            customPlot->axisRect()->setupFullAxesBox();

            // make left and bottom axes transfer their ranges to right and top axes:
            connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
            connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
        }
    }

    void updatePlot(QCustomPlot *customPlot, double value){
        double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
        double lastPointKey = 0;
        if (key - lastPointKey > 0.01){

            customPlot->graph(0)->addData(key, value);
            customPlot->graph(0)->removeDataBefore(key-8);
            customPlot->graph(0)->rescaleValueAxis();

            lastPointKey = key;
        }

        //make key axis range scroll with the data (at a constant range size of 8):
        customPlot->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
        customPlot->replot();
    }

    void updatePlot(QCustomPlot *customPlot, double throttle, double brake){
        //updating accelerator plot
        double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
        double lastPointKey = 0;
        if (key - lastPointKey > 0.01){

            customPlot->graph(0)->addData(key, throttle);
            customPlot->graph(0)->removeDataBefore(key-8);
            customPlot->graph(0)->rescaleValueAxis();

            customPlot->graph(1)->addData(key, brake);
            customPlot->graph(1)->removeDataBefore(key-8);
            customPlot->graph(1)->rescaleValueAxis();

            lastPointKey = key;
        }

        //make key axis range scroll with the data (at a constant range size of 8):
        customPlot->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
        customPlot->replot();
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 33;
        int dy = new_size.height() / 33;

        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);

        splitter->setGeometry(1*dx,1*dy,new_size.width()-dx,new_size.height()-dy);

        rpm->setMinimumHeight(new_size.height()/ 6);
        speed->setMinimumHeight(new_size.height()/ 6);
        gear->setMinimumHeight(new_size.height()/ 6);
        accelerator->setMinimumHeight(new_size.height()/ 6);
    }

    void updateDataSlot(int rpm, int gear, double speed, int throttle, int brake){
        updatePlot(this->rpm,(double) rpm);
        updatePlot(this->gear,(double) gear);
        updatePlot(this->speed,(double) speed);
        updatePlot(accelerator,(double) throttle, (double) brake);
    }
};

/*  ________
 * |__|_____|
*/
//displays top toolbar with info about race status, penalty, lap, position, size (32:8)
class LiveToolbar : public QWidget{
    Q_OBJECT

public:
    //class @
    //main widget
    QWidget *central;

    //labels
    //flag no needs title
    QLabel *penalty_label;
    QLabel *position_label;
    QLabel *lap_label;

    //widgets
    QLabel *flag_label;
    QPixmap flag;
    QLabel *penalty;
    QLabel *position;
    QLabel *lap;

    //frames
    QFrame *flag_container;
    QFrame *penalty_container;
    QFrame *position_container;
    QFrame *lap_container;
    QFrame *down_container;

    LiveToolbar(QWidget *parent){
        central = new QWidget(parent);
        setupLabels();
        setupTexts();
        setupFrames();
   }

    void setupLabels(){
        penalty_label = new QLabel("Penalties", central);
        position_label = new QLabel("Rank", central);
        lap_label = new QLabel("Lap", central);

        penalty_label->setAlignment(Qt::AlignCenter);
        position_label->setAlignment(Qt::AlignCenter);
        lap_label->setAlignment(Qt::AlignCenter);
    }

    void setupTexts(){
        penalty = new QLabel("Drive through penalty.\n5 laps", central);
        position = new QLabel("1", central);
        lap = new QLabel("44/89", central);

        penalty->setAlignment(Qt::AlignCenter);
        position->setAlignment(Qt::AlignCenter);
        lap->setAlignment(Qt::AlignCenter);

        //image
        flag_label = new QLabel(central);
        flag_label->setAlignment(Qt::AlignCenter);
    }

    void setupFrames(){
        flag_container = new QFrame(central);
        penalty_container = new QFrame(central);
        position_container = new QFrame(central);
        lap_container = new QFrame(central);
        down_container = new QFrame(central);

        flag_container->setFrameStyle(QFrame::Box);
        penalty_container->setFrameStyle(QFrame::Box);
        position_container->setFrameStyle(QFrame::Box);
        lap_container->setFrameStyle(QFrame::Box);
        down_container->setFrameStyle(QFrame::Box);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 32;
        int dy = new_size.height() / 8;

        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);

        //containers
        down_container->setGeometry(4*dx,3*dy, 28*dx, 5*dy);
        flag_container->setGeometry(0,0, 4*dx, 8*dy);
        lap_container->setGeometry(4*dx,0, 3*dx, 8*dy);
        position_container->setGeometry(7*dx,0, 3*dx, 8*dy);
        penalty_container->setGeometry(10*dx,0, 22*dx, 8*dy);

        //flag image
        flag = QPixmap("img/sc_board.png");
        flag = flag.scaledToWidth(4*dx);
        flag_label->setPixmap(flag);
        flag_label->setGeometry(0,0, 4*dx, 8*dy);

        //titles
        lap_label->setGeometry(4*dx,0, 3*dx, 2*dy);
        position_label->setGeometry(7*dx,0, 3*dx, 2*dy);
        penalty_label->setGeometry(10*dx,0, 22*dx, 2*dy);

        //values
        lap->setGeometry(4*dx,3*dy, 3*dx, 5*dy);
        position->setGeometry(7*dx,3*dy, 3*dx, 5*dy);
        penalty->setGeometry(10*dx,3*dy, 22*dx, 5*dy);

        //texts
        QFont resized_font;
        resized_font.setPointSize(13*dx/20);

        penalty_label->setFont(resized_font);
        position_label->setFont(resized_font);
        lap_label->setFont(resized_font);
        penalty->setFont(resized_font);
        position->setFont(resized_font);
        lap->setFont(resized_font);
    }

    void update(){
        //
    }
};

/* _____
 *|_|_|_|
 *|     |
 *|_____|
*/
//displays live stats of lap time and sectors, size (32:32)
class LapTimeStats : public QWidget{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    QFrame *container;

    QLabel *sec1;
    QLabel *sec2;
    QLabel *sec3;
    QLabel *sec1_title;
    QLabel *sec2_title;
    QLabel *sec3_title;
    QFrame *sec1_container;
    QFrame *sec2_container;
    QFrame *sec3_container;
    QFrame *down_container;
    QCustomPlot *laptime;

    LapTimeStats(QWidget *parent){
        central = new QWidget(parent);
        setupLabels();
        setupFrames();
        setupPlot();
    }

    void setupLabels(){
        sec1 = new QLabel("23' 419", central);
        sec2 = new QLabel("18' 653", central);
        sec3 = new QLabel("26' 218", central);
        sec1_title = new QLabel("Sector 1", central);
        sec2_title = new QLabel("Sector 2", central);
        sec3_title = new QLabel("Sector 3", central);

        sec1->setAlignment(Qt::AlignCenter);
        sec2->setAlignment(Qt::AlignCenter);
        sec3->setAlignment(Qt::AlignCenter);
        sec1_title->setAlignment(Qt::AlignCenter);
        sec2_title->setAlignment(Qt::AlignCenter);
        sec3_title->setAlignment(Qt::AlignCenter);
    }

    void setupFrames(){
        sec1_container = new QFrame(central);
        sec2_container = new QFrame(central);
        sec3_container = new QFrame(central);
        down_container = new QFrame(central);
        container = new QFrame(central);

        sec1_container->setFrameStyle(QFrame::Box);
        sec2_container->setFrameStyle(QFrame::Box);
        sec3_container->setFrameStyle(QFrame::Box);
        down_container->setFrameStyle(QFrame::Box);
        container->setFrameStyle(QFrame::Box);
    }

    void setupPlot(){
        laptime = new QCustomPlot(central);

        //4 plots: sec1, sec2, sec3 + laptime
        laptime->addGraph();//sec1
        laptime->graph(0)->setPen(QPen(QColor("#FF0000")));

        laptime->addGraph();//sec2
        laptime->graph(1)->setPen(QPen(QColor("#00FF00")));

        laptime->addGraph();//sec3
        laptime->graph(2)->setPen(QPen(QColor("#0000FF")));

        laptime->addGraph();//laptime
        laptime->graph(3)->setPen(QPen(Qt::black));

        laptime->xAxis->setTickLabelType(QCPAxis::ltDateTime);
        laptime->xAxis->setDateTimeFormat("hh:mm:ss");
        laptime->xAxis->setAutoTickStep(false);
        laptime->xAxis->setTickStep(2);
        laptime->axisRect()->setupFullAxesBox();
        laptime->xAxis->setLabel("SECTOR 1,2,3 / LAPTIME");

        //make left and bottom axes transfer their ranges to right and top axes:
        connect(laptime->xAxis, SIGNAL(rangeChanged(QCPRange)), laptime->xAxis2, SLOT(setRange(QCPRange)));
        connect(laptime->yAxis, SIGNAL(rangeChanged(QCPRange)), laptime->yAxis2, SLOT(setRange(QCPRange)));
    }

    void updatePlot(QCustomPlot *customPlot, double sec1, double sec2, double sec3){
        //updating accelerator plot
        double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
        double lastPointKey = 0;
        if (key - lastPointKey > 0.01){

            //add new data
            customPlot->graph(0)->addData(key, sec1);
            customPlot->graph(0)->removeDataBefore(key-8);

            customPlot->graph(1)->addData(key, sec2);
            customPlot->graph(1)->removeDataBefore(key-8);

            customPlot->graph(2)->addData(key, sec3);
            customPlot->graph(2)->removeDataBefore(key-8);

            customPlot->graph(3)->addData(key, sec1 + sec2 + sec3);
            customPlot->graph(3)->removeDataBefore(key-8);

            //rescale axis
            customPlot->graph(0)->rescaleValueAxis();
            customPlot->graph(3)->rescaleValueAxis();

            lastPointKey = key;
        }

        //make key axis range scroll with the data (at a constant range size of 8):
        customPlot->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
        customPlot->replot();
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 32;
        int dy = new_size.height() / 32;

        //layout
        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);
        container->setGeometry(0,0,new_size.width(), new_size.height());

        //frames
        sec1_container->setGeometry(dx, dy, 10*dx, 6*dy);
        sec2_container->setGeometry(11*dx, dy, 10*dx, 6*dy);
        sec3_container->setGeometry(21*dx, dy, 10*dx, 6*dy);
        down_container->setGeometry(dx, 3*dy, 30*dx, 4*dy);

        //texts
        sec1->setGeometry(1*dx,3*dy,10*dx,3*dy);
        sec2->setGeometry(11*dx,3*dy,10*dx,3*dy);
        sec3->setGeometry(21*dx,3*dy,10*dx,3*dy);
        sec1_title->setGeometry(1*dx,1*dy,10*dx,2*dy);
        sec2_title->setGeometry(11*dx,1*dy,10*dx,2*dy);
        sec3_title->setGeometry(21*dx,1*dy,10*dx,2*dy);

        QFont resized_font;
        resized_font.setPointSize(13*dx/20);

        sec1->setFont(resized_font);
        sec2->setFont(resized_font);
        sec3->setFont(resized_font);
        sec1_title->setFont(resized_font);
        sec2_title->setFont(resized_font);
        sec3_title->setFont(resized_font);

        //plot
        laptime->setGeometry(dx, 9*dy, 30*dx, 22*dy);
    }

    void update(double sec1, double sec2, double sec3){
        updatePlot(this->laptime, sec1, sec2, sec3);
    }
};

/*  _____
 * |     |
 * |_____|
 * |_____|
*/
//displays pitstop manager, size (32 : )
class PitStopManager : public QWidget{
    Q_OBJECT
public:
    //class @
    QWidget *central;
    QFrame *container;

    QLabel *pit_legend;
    QLabel *lap_legend;
    QLabel *minimap_room;
    QPixmap minimap;
    QLabel *result_prediction;
    QLabel *result_best;
    QSpinBox *pitstop_1;
    QSpinBox *pitstop_2;
    QSpinBox *pitstop_3;
    QSpinBox *pitstop_4;
    QSpinBox *pitstop_5;
    QPushButton *predict_button;
    QPushButton *best_pit_button;
    //QTextEdit to insert lap of future pitstop
    //dialogs to predict best time to pit

    PitStopManager(QWidget *parent){
        central = new QWidget(parent);
        setupLayout();
    }

    void setupLayout(){
        //main frame
        container = new QFrame(central);
        container->setFrameStyle(QFrame::Box);

        //labels
        pit_legend = new QLabel("Pitstops\n\n1\n\n2\n\n3\n\n4\n\n5", central);
        lap_legend = new QLabel("Lap", central);
        result_prediction = new QLabel("89'423", central);
        result_best = new QLabel("89'423\"", central);
        pitstop_1 = new QSpinBox(central);
        pitstop_2 = new QSpinBox(central);
        pitstop_3 = new QSpinBox(central);
        pitstop_4 = new QSpinBox(central);
        pitstop_5 = new QSpinBox(central);

        //create widgets
        minimap_room = new QLabel(central);
        predict_button = new QPushButton("Predict", central);
        best_pit_button = new QPushButton("Find best", central);

        //edit features
        pit_legend->setAlignment(Qt::AlignCenter);
        lap_legend->setAlignment(Qt::AlignCenter);
        result_best->setAlignment(Qt::AlignCenter);
        result_prediction->setAlignment(Qt::AlignCenter);
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 22;
        int dy = new_size.height() / 19;

        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);
        container->setGeometry(0,0, new_size.width(), new_size.height());

        minimap = QPixmap("img/map_monza");
        minimap = minimap.scaledToWidth(9*dx);
        minimap_room->setPixmap(minimap);
        minimap_room->setGeometry(12*dx, dy, 9*dx, 11*dy);

        pit_legend->setGeometry(dx, dy, 4*dx, 17*dy);
        lap_legend->setGeometry(7*dx, dy, 3*dx, 2*dy);
        pitstop_1->setGeometry(7 * dx, 4 * dy, 4*dx, 2*dy);
        pitstop_2->setGeometry(7 * dx, 7 *dy, 4*dx, 2*dy);
        pitstop_3->setGeometry(7 * dx, 10*dy, 4*dx, 2*dy);
        pitstop_4->setGeometry(7 * dx, 13*dy, 4*dx, 2*dy);
        pitstop_5->setGeometry(7 * dx, 16*dy, 4*dx, 2*dy);

        QFont resized_font;
        resized_font.setPointSize(dy);
        pit_legend->setFont(resized_font);
        lap_legend->setFont(resized_font);
        pitstop_1->setFont(resized_font);
        pitstop_2->setFont(resized_font);
        pitstop_3->setFont(resized_font);
        pitstop_4->setFont(resized_font);
        pitstop_5->setFont(resized_font);

        predict_button->setGeometry(12*dx, 13*dy, 4*dx, 2*dy);
        best_pit_button->setGeometry(17*dx, 13*dy, 4*dx, 2*dy);

        result_prediction->setGeometry(12*dx, 16*dy, 4*dx, 2*dy);
        result_best->setGeometry(17*dx, 16*dy, 4*dx, 2*dy);
    }
};

/* ___________
 *|_|         |
 *|_|         |
 *|_|_________|
*/
//title, value and plot of live chart of 1 data value
class PlotterOne : public QWidget
{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    QFrame *container;

    QLabel *title;
    QCustomPlot *plot;
    QLabel *current;
    QCheckBox *is_draw;

    PlotterOne(QString title, QWidget *parent){
        central = new QWidget(parent);
        this->title = new QLabel(title, central);
        setupLayout();
        setupPlot(plot);
    }

    void setupLayout(){
        //create widgets
        container = new QFrame(central);
        current = new QLabel("0", central);
        is_draw = new QCheckBox(central);
        this->plot = new QCustomPlot(central);

        //edit features
        container->setFrameStyle(QFrame::Box);
        title->setAlignment(Qt::AlignCenter);
        current->setAlignment(Qt::AlignCenter);
        is_draw->setChecked(true);
        QObject::connect(is_draw, SIGNAL(toggled(bool)), this->plot, SLOT(setVisible(bool)));
    }

    void setupPlot(QCustomPlot *customPlot){
        customPlot->addGraph();
        customPlot->graph(0)->setPen(QPen(Qt::black));

        customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
        customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
        customPlot->xAxis->setAutoTickStep(false);
        customPlot->xAxis->setTickStep(2);
        customPlot->axisRect()->setupFullAxesBox();

        // make left and bottom axes transfer their ranges to right and top axes:
        connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
        connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    }

    void setValue(double value){
        this->current->setText(QString::number(value));
        updatePlot(this->plot, value);
    }

    void updatePlot(QCustomPlot *customPlot, double value){
        double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
        double lastPointKey = 0;
        if (key - lastPointKey > 0.01){

            customPlot->graph(0)->addData(key, value);
            customPlot->graph(0)->removeDataBefore(key-8);
            customPlot->graph(0)->rescaleValueAxis();

            lastPointKey = key;
        }

        //make key axis range scroll with the data (at a constant range size of 8):
        customPlot->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
        customPlot->replot();
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 36;
        int dy = new_size.height() / 10;

        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);
        container->setGeometry(0,0, new_size.width(), new_size.height());

        title->setGeometry(dx, dy, 3 * dx, 2 * dy);
        current->setGeometry(dx, 3*dy, 3 * dx, 2*dy);
        is_draw->setGeometry(2 *dx, 6*dy, dx, 2 *dy);

        plot->setGeometry(5*dx, dy, 30*dx, 8*dy);
    }
};

/* ___________
 *|_|         |
 *|_|         |
 *|_|_________|
*/
//title, value and plot of live chart of 1 data value
class PlotterFour : public QWidget
{
    Q_OBJECT

public:
    //class @
    QWidget *central;
    QFrame *container;

    QLabel *title;
    QCustomPlot *plot;
    QCheckBox *is_draw;

    PlotterFour(QString title, QWidget *parent){
        central = new QWidget(parent);
        this->title = new QLabel(title, central);
        setupLayout();
        setupPlot(plot);
    }

    void setupLayout(){
        //create widgets
        container = new QFrame(central);
        is_draw = new QCheckBox(central);
        this->plot = new QCustomPlot(central);

        //edit features
        container->setFrameStyle(QFrame::Box);
        title->setAlignment(Qt::AlignCenter);
        is_draw->setChecked(true);
        QObject::connect(is_draw, SIGNAL(toggled(bool)), this->plot, SLOT(setVisible(bool)));
    }

    void setupPlot(QCustomPlot *customPlot){
        customPlot->addGraph();
        customPlot->graph(0)->setPen(QPen(Qt::yellow));

        customPlot->addGraph();
        customPlot->graph(1)->setPen(QPen(Qt::blue));

        customPlot->addGraph();
        customPlot->graph(2)->setPen(QPen(Qt::red));

        customPlot->addGraph();
        customPlot->graph(3)->setPen(QPen(Qt::green));

        customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
        customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
        customPlot->xAxis->setAutoTickStep(false);
        customPlot->xAxis->setTickStep(2);
        customPlot->axisRect()->setupFullAxesBox();

        //make left and bottom axes transfer their ranges to right and top axes:
        connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
        connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    }

    void setValue(double value0, double value1, double value2, double value3){
        updatePlot(this->plot, value0, value1, value2, value3);
    }

    void updatePlot(QCustomPlot *customPlot, double value0, double value1, double value2, double value3){
        double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
        double lastPointKey = 0;
        if (key - lastPointKey > 0.01){

            customPlot->graph(0)->addData(key, value0);
            customPlot->graph(0)->removeDataBefore(key-8);
            customPlot->graph(0)->rescaleValueAxis();

            customPlot->graph(1)->addData(key, value1);
            customPlot->graph(1)->removeDataBefore(key-8);
            customPlot->graph(1)->rescaleValueAxis();

            customPlot->graph(2)->addData(key, value2);
            customPlot->graph(2)->removeDataBefore(key-8);
            customPlot->graph(2)->rescaleValueAxis();

            customPlot->graph(3)->addData(key, value3);
            customPlot->graph(3)->removeDataBefore(key-8);
            customPlot->graph(3)->rescaleValueAxis();

            lastPointKey = key;
        }

        //make key axis range scroll with the data (at a constant range size of 8):
        customPlot->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
        customPlot->replot();
    }

    void resize(QSize new_size){
        int dx = new_size.width() / 36;
        int dy = new_size.height() / 10;

        central->setMinimumSize(new_size);
        central->setMaximumSize(new_size);
        container->setGeometry(0,0, new_size.width(), new_size.height());

        title->setGeometry(dx, dy, 3 * dx, 2 * dy);
        is_draw->setGeometry(2 *dx, 6*dy, dx, 2 *dy);

        plot->setGeometry(5*dx, dy, 30*dx, 8*dy);
    }
};


#endif
